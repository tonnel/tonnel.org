import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

export default function({ app, route }, inject) {
  const an = {
    isMobile: () => Vue.prototype.$vuetify.breakpoint.smAndDown,
    isTablet: () => Vue.prototype.$vuetify.breakpoint.mdAndDown,
    isSmall: () => Vue.prototype.$vuetify.breakpoint.width < 1600
  }

  inject('an', an)
}
