module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Tonnel MTProxy Network',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Tonnel Network is a set of MTProxy servers spreaded worldwide. Cross-region load balancing makes your Telegram connection fast and stable.'
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content:
          'MTProxy, MTProto, fast mtproxy, telegram access, global mtproxy'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/image/png',
        href: '/img/favicon16.png',
        sizes: '16x16'
      },
      {
        rel: 'icon',
        type: 'image/image/png',
        href: '/img/favicon32.png',
        sizes: '32x32'
      },
      {
        rel: 'icon',
        type: 'image/image/png',
        href: '/img/favicon96.png',
        sizes: '96x96'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },
  modules: [
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-120533849-1'
      }
    ]
  ],
  plugins: ['~/plugins/vuetify.js'],
  css: ['~/assets/style/app.styl'],

  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['~/plugins/vuetify.js'],
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
